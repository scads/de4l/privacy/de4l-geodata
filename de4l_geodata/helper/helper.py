"""Provides helper functions for test classes.
"""
from os import walk

import numpy as np


def read_files_from_path(path):
    paths = []
    for (dirpath, _, files) in walk(path):
        for file in files:
            if file[-3:] == 'plt':
                paths.append(dirpath + '/' + file)
    return paths


def get_digits(value, number_of_decimals):
    """
    Returns the whole-number digits of value and number_of_decimals values after the comma.

    Parameters
    ----------
    value : float
        The value to apply this method to.
    number_of_decimals : int
        The number of decimal values to return.

    Returns
    -------
    int
        The whole-number digits of 'value' and its decimal digits up to 'number_of_decimals'.
    """
    return np.floor(value * np.power(10, number_of_decimals))
